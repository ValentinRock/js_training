const array = [9, 3, 5, 4, 3, 2, 5, 7, 3, 5, 5,];
let count = 0;

const linearSearch = (n, array) => {
  for (let i = 0; i < array.length; i++) {
      count += 1;
      if(array[i] === n) {
          return i;
      }
  }

  return null;
}

console.log(linearSearch(2, array), `count = ${count}`);