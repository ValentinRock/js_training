const fibonachi = (n) => {
    if (n < 0) {
        return 0;
    }

    if (n === 1) {
        return n + 1;
    }

    if (n > 1) {
        return fibonachi(n - 1);
    }
}

console.log(fibonachi(10))