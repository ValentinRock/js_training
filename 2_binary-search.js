const array = [9, 3, 5, 4, 3, 2, 5, 7, 3, 5, 5,];
let count = 0;

const binarySearch = (n, array) => {
    let start = 0;
    let end = array.length;
    let middle;
    let found = false;
    let position = -1;

    while (found === false && start <= end) {
        count += 1;
        middle = Math.floor((start + end) / 2);

        if (array[middle] === n) {
            found = true;
            position = middle;
            return position;
        }

        if (n < array[middle]) {
            end = middle - 1;
        } else {
            start = middle + 1;
        }

        return position;
    }
}

console.log(binarySearch(5, array), `count = ${count}`);